#!/usr/bin/env perl

use 5.36.1;
use Data::Dumper;

my $tags = { 1 => { slug => "tag1",
                    name => "Tag 1"},
             2 => { slug => "tag2",
                    name => "Tag 2"},
             3 => { slug => "tag3",
                    name => "Tag 3"},
             4 => { slug => "tag4",
                    name => "Tag 4"},
             5 => { slug => "tag5",
                    name => "Tag 5"},
         };

my $post_tags = [1,2,5];


# my $posttagnames should be an array of names like ['Tag 1', 'Tag 2', 'Tag 5']
#my $post_tag_names =  [$tags->{1}->{name}];

#my @names = map $tags{$_}{name}, @keys;

#my $post_tag_names = [ map { $_->{name} } values @$tags ];
my $post_tag_names = [ map { $tags->{$_}{name} } @$post_tags ];
say Dumper $post_tag_names;
#my $post_tag_names = [ map { $tags->{$_}{name} } keys @$tags ];


#say Dumper @names;


#say Dumper $post_tags;
#say Dumper $post_tag_names;

# 18:46:19        integral | k-man: hash slices: @values = @hash{@keys}; @names = map $_->{name}, @hash{@keys}; Then again, you can just use map without a slice too:
#                          | @names = map $hash{$_}{name}, @keys
# -- Fri, 09 Jun 2023 --
# 00:33:39           ology | my $post_tag_names = [ map { $_->{name} } keys @$tags ];
# 00:33:45           ology | k-man: ^
# 00:34:22           ology | my $post_tag_names = [ map { $tags->{$_}{name} } keys @$tags ];
# 00:34:39           ology | There we go.
# 00:35:36           ology | Or: my $post_tag_names = [ map { $_->{name} } values @$tags ];
# 03:03:34           Leolo | @values = @hash{ @names }
# 03:03:46           Leolo | or @values = @{ $hashref }{ @names }
# 03:03:55           Leolo | k-man : these are called hash slices

